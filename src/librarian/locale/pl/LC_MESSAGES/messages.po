# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2024-11-20 14:54+0100\n"
"Last-Translator: Radek Czajka <rczajka@rczajka.pl>\n"
"Language-Team: \n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 3.4.2\n"

#: src/librarian/builders/html.py:136
msgid "translated by"
msgstr "tłum."

#: src/librarian/builders/html.py:157
msgid "Footnotes"
msgstr "Przypisy"

#: src/librarian/elements/footnotes/__init__.py:112
msgid "author's footnote"
msgstr "przypis autorski"

#: src/librarian/elements/footnotes/__init__.py:120
msgid "translator's footnote"
msgstr "przypis tłumacza"

#: src/librarian/elements/footnotes/__init__.py:128
msgid "editor's footnote"
msgstr "przypis redakcyjny"

#: src/librarian/elements/footnotes/__init__.py:136
msgid "source editor's footnote"
msgstr "przypis edytorski"

#: src/librarian/elements/footnotes/__init__.py:144
msgid "traditional footnote"
msgstr "przypis tradycyjny"
